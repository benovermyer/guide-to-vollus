The City of Vollus
==================

The population is about 60% humans, 25% tieflings, refugees from the
kingdom of Lindus which fell due to internal strife 20 years ago. 15%
halfling, gnome, orcs, "other". Tieflings are a permanent underclass and
viewed as disdainful, corrupt, base and sinful though a few have managed
to claw their way to higher station as merchants or through criminal
enterprises, only securing preexisting contempt of the average citizen.
It is very common to have some orc blood in one's family, and orcs are
culturally viewed as hard working (if simple) paragons of the working
class, save for House Ostmoor. There are few elves, who are viewed as
suspicious and a bit alien. Halflings and gnomes are viewed as mostly
harmless. There are no dwarves, though it believed to go into the
mountains would be suicide as the vile monsterous creatures would devour
you or worse - pull you down into their dark kingdoms for Gods know
what.

The immediate city is more 1800's than medieval, - guns and gadgetry are
just beginning to show up, though very unreliable and rare curiosities.
The city, and surrounding lands are mountainous, countries are
balkanized - cut off, warlike, and very suspicious of each other. The
general cultures are flavored Eastern European and a bit Gothic. Think
Gotham, Prague, Hungary, Russia. The city is protected by several rings
of high walls from horrors that might prey on good folk. A large river,
the Skoronova runs through the city.

Outside the city is a place only the hardiest dare venture. There are
constant rumors of undead, werewolves, fae and unknown creatures eating
errant children and travelers. Strong woodsmen and hunters venture out,
but not most people with any sense. Occasional smoke from a distant
mountain is a clear harbinger of deadly dragons, though no one would
dare try to find one.

Culture/ History
----------------

Arcane magic was viewed suspiciously already, though practiced secretly
as a useful art by the educated noble families in the city. Four years
ago the already failing King and a council of nobles were all killed by
an arcane assassin. The city populace was bloodthirsty and already
teetering on destructive riots over taxes, a bad harvest and societal
strife. The noble families wisely served the mob some red meat - there
was a huge purge as arcane casters were tried, broken on the wheel,
hung, or mercifully beheaded to protect society from their evil powers.
Now, it is illegal to use arcane magic though some suspect the noble
families maintain powerful forbidden arcane secrets themselves and are
just very discreet about it.

The noble houses had founders that exemplified connection to the old
magic, many of their heirs demonstrate this affinity to this day. This
is not arcane magic necessarily. Peasants living in a noble's district
often take pride in this magic symbolically. Examples are life and
agriculture for House Kormos, or fire and militarism for House Dragos.
Each house also has a domain of control or competency leading to their
power and wealth, as well as a district they are responsible for. It is
a constant challenge to keep the peasants from rebelling over high
taxes, accusations of witchcraft, anti-tiefling riots, poxes, nobles
that have lost public confidence due to depravity, and many other
challenges.

Since the death of King Gregori, the noble houses have been locked in a
struggle to see who will ascend to power. They constantly making
alliances and enemies of each other in endless brinkmanship. Anytime one
house gets too powerful, a handful of other houses come together to
oppose them.

Religions
---------

The church of [Lathander](../lathander) holds the most sway in town, it
largely secured power 4 years ago, helping to cleanse the city of the
arcane assassins and tiefling co-conspirators. It owes no allegiance to
the noble houses, only to rooting out sin and depravity and protecting
society. There are rumors of dark cults and pacts with the Dead God
Myrkul and the Raven Queen, another reason protection and salvation of
one's immortal soul binds the common folk to the church of the
Morninglord. It is rumored the church has secret police to protect the
city against the depredations of sinners, wizards, warlocks and witches,
demon and devil worshippers - common and noble alike. The temptation to
arcane power is an ever present evil.

Other benign religions like [Bahamut](../bahamut) and Pelor are tolerated
in town, though considered a bit odd.

Outside the city, wild folk and barbarians, and dwarves are said to
revere Malar, the Beastlord, who tells his followers to rend victims
with tooth and claw, cannibalizing good folk to devour the power held in
the flesh and souls of innocents.

Government
----------

King Gregori, upon his death, had no heirs. The traditional structure
was the King, his council of advising noble house heads. Each noble
house ran a district, who also had a small council of commoners who
advised them on their district. Since the death of the King, however,
the noble houses meet in council and take majority votes to decide
city-wide matters, jealously guarding their own district's power. House
Dragos is unique in that they run the city gates and wall as part of
their district. The church had an advisory role to the King on spiritual
matters, but in his latter years was at odds with his desire to remarry
to create an heir, causing a rift. Because of this, and their handling
of the plague 20 years ago, the legal power of the church had been on
the wane. Though they have no formal role politically, they have pursued
power through public sentiment. The average citizen of Vollus thinks
very highly of the Church, and may have heard rumors of the Nobles
bickering over tax levies and other petty things. Family heads generally
follow the oldest male heir as tradition, but abdications and other
arrangements are considered a family affair if it seems best for the
family, due to incompetence or infirmity. Houses are free to send any
member they wish to vote at council, and sometimes switch up members due
to illness, travel or to give an up and coming scion a taste of
politics. It generally requires 4 out of 7 votes to make a law official.
Houses may abstain from voting but rarely do. Most houses defer to other
Houses sphere of expertise, and would not generally pass laws at
dictating how Dragos protects the wall for example, because it could
cause chaos.
